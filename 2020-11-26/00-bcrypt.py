import bcrypt

#clave = b'123456'
clave = '123456'.encode('utf-8')

salt = bcrypt.gensalt()

claveHash = bcrypt.hashpw(clave, salt)

print(salt)
print(claveHash)
