import bcrypt

# clave = b'123456'
clave = '123456'.encode('utf-8')

salt = bcrypt.gensalt()

claveHash = bcrypt.hashpw(clave, salt)


if bcrypt.checkpw(b'123456', claveHash):
    print('Coincide')
else:
    print('No coincide')
