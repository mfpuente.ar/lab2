import sqlite3
import bcrypt

# Conectar a base de datos (se crea el archivo si no existe)
conexion = sqlite3.connect('2020-11-09/01-base.db')

# Operaciones con la base de datos

# Generar contraseña
clave = 'abcd'.encode('utf-8')
salt = bcrypt.gensalt()
claveHash = bcrypt.hashpw(clave, salt)

# Crear en base de datos usuario
cursor = conexion.cursor()
cursor.execute("""INSERT INTO Usuarios
                  (id,nombre_usuario,nombre_completo,contraseña,email)
                  VALUES (2,'msosa','Manuel Sosa','""" + claveHash.decode('utf-8') + """','correojuan@correo.com');"""
               )
conexion.commit()

conexion.close()
