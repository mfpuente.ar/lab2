from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-26/01-combo_box.ui", self)
        self.lista.currentIndexChanged.connect(self.on_lista_changed)

    def on_lista_changed(self, index):
        indice = self.lista.currentIndex()
        self.eleccion.setText(str(indice) + ' - ' + self.lista.currentText())

# Agregar un item
    self.lista.addItem("texto")

# Editar un item
    self.lista.setItemText(index, "texto")

# Quitar un item
    self.lista.removeItem(index)

# Quitar todo
    self.lista.clear()


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
