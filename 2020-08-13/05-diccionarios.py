dic = {
    "banana": "amarilla",
    "manzana": "roja",
    "pera": "verde"
}

print(dic)

print(dic["banana"])

dic["banana"] = 10

print(list(dic.values()))
print(list(dic.keys()))

dic.pop("manzana")

print(dic)
