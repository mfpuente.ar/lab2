# Clase persona
# Parametros: nombre, apellido, edad, dni
# Metodos: nombre_completo() -> apellido, nombre

class Persona:
    def __init__(self, nombre, apellido, edad, dni):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.dni = dni

    def nombre_completo(self):
        return self.apellido + ', ' + self.nombre

    def __str__(self):
        return '{0}, {1}, {2}, {3}'.format(self.apellido, self.nombre, self.edad, self.dni)
