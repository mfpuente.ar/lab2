# operaciones con datetime
from datetime import datetime, timedelta

fecha1 = "2020-08-31"
fecha2 = "2020-12-25"

dt1 = datetime.strptime(fecha1, "%Y-%m-%d")
dt2 = datetime.strptime(fecha2, "%Y-%m-%d")

diff = dt2 - dt1
print("Faltan para navidad: ", diff)


td = timedelta(5)

print("Dentro de 5 dias es: ", dt1 + td)
