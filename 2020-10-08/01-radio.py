from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-08/01-radio.ui", self)
        self.boton.clicked.connect(self.on_clicked)

    def on_clicked(self):
        if self.opcion1.isChecked():
            self.etiqueta.setText("Se elige la opcion 1")
        elif self.opcion2.isChecked():
            self.etiqueta.setText("Se elige la opcion 2")
        elif self.opcion3.isChecked():
            self.etiqueta.setText("Se elige la opcion 3")
        else:
            self.etiqueta.setText("No se eligio opcion")


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
