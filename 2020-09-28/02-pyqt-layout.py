from PyQt5.QtWidgets import QApplication, QMainWindow, QHBoxLayout, QLabel, QWidget

# Crear aplicacion
app = QApplication([])

# Crear ventana principal
ventana = QMainWindow()
ventana.setWindowTitle('Primer programa')

# Agregar componentes
widget = QWidget()
layout = QHBoxLayout()
label = QLabel("Hola mundo!")
label2 = QLabel("Chau mundo!")

layout.addWidget(label)
layout.addWidget(label2)

widget.setLayout(layout)
ventana.setCentralWidget(widget)
ventana.show()

# Ejecutar aplicacion
app.exec_()
