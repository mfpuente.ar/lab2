from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-09-28/ventana.ui", self)


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
