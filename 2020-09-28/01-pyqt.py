from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QLabel, QWidget

# Crear aplicacion
app = QApplication([])

# Crear ventana principal
ventana = QMainWindow()
ventana.setWindowTitle('Primer programa')
ventana.show()

# Ejecutar aplicacion
app.exec_()
