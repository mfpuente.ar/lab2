# Valor por defecto
def suma(x=1, y=8):
    print('valor de x:', x)
    print('valor de y:', y)
    return x + y


print("La suma es:", suma(3, 5))
print("La suma es:", suma())
print("La suma es:", suma(y=2))


def saludo(pais="Argentina"):
    return "Hola, soy de " + pais


print(saludo())
print(saludo("Chile"))
