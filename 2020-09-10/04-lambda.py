from persona import Persona


def func_edad(x):
    return x.edad


lista = [Persona("Apellido", "nombre", 44, "333434"), Persona(
    "Apellido", "nombre", 8, "333434"), Persona("Apellido", "nombre", 99, "333434")]

lista.sort(key=lambda x: x.edad)
# lista.sort(key=func_edad)

for item in lista:
    print(item)
