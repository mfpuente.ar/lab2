lista = [2, 4, 6, 2, 7, 8]

# cuadrados = []
# for numero in lista:
# cuadrados.append(numero ** 2)


def cuad_func(num):
    return num ** 2


cuadrados = list(map(cuad_func, lista))

print(cuadrados)
