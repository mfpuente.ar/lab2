edades = [13, 12, 5, 16, 23, 24, 56, 45, 10, 78, 45, 39]


def may_func(edad):
    return edad >= 18


mayores = list(filter(may_func, edades))
menores = list(filter(lambda edad: edad < 18, edades))

print(mayores)
print(menores)
