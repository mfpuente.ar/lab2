# Class: Canciones
# Parametro: listado de canciones
# Metodos:
#   Agregar: agrega una cancion
#   listar_por_interprete: Muestra todas las canciones de un interprete(formato resumido)
#   listar_por_estilo: Muestra todas las canciones de un estilo (formato resumido)
#   listar_todas: Muestra todas las canciones (formato completo)
#   quitar: quitar una cancion por titulo e interprete
