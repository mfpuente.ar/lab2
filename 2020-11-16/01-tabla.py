from PyQt5.QtWidgets import QMainWindow, QApplication, QTableWidgetItem, QHeaderView
from PyQt5 import uic
import sqlite3


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-11-16/01-tabla.ui", self)
        self.conexion = sqlite3.connect('2020-11-09/00-base.db')
        self.cursor = self.conexion.cursor()

        self.usuarios.setColumnCount(3)
        self.usuarios.setHorizontalHeaderLabels(
            ('Nombre de usuario',  'Nombre completo', 'E-mail'))
        header = self.usuarios.horizontalHeader()
        for i in range(2):
            header.setSectionResizeMode(i, QHeaderView.ResizeToContents)
        self.verUsuarios.clicked.connect(self.on_ver_usuarios)

    def on_ver_usuarios(self):
        self.cursor.execute('SELECT * FROM Usuarios')

        # self.cursor.execute('UPDATE Usuarios set nombreUsuario="cambio",email="sdf" WHERE id=3')

        usuarios = self.cursor.fetchall()
        self.usuarios.setRowCount(0)
        for usuario in usuarios:
            fila = usuarios.index(usuario)
            self.usuarios.insertRow(fila)
            self.usuarios.setItem(fila, 0, QTableWidgetItem(usuario[1]))
            self.usuarios.setItem(fila, 1, QTableWidgetItem(usuario[3]))
            self.usuarios.setItem(fila, 2, QTableWidgetItem(usuario[4]))

    def closeEvent(self, event):
        self.conexion.close()


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
