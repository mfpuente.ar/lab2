from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
import sqlite3


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-11-16/00-base.ui", self)
        self.conexion = sqlite3.connect('2020-11-09/00-base.db')
        self.cursor = self.conexion.cursor()
        self.agregar.clicked.connect(self.on_agregar)

    def on_agregar(self):
        self.cursor.execute("""INSERT INTO Usuarios 
                       (nombre_usuario,nombre_completo,contraseña,email)
                       VALUES ('usuario3','pedro','clave2','pedro@correo.com');"""
                            )
        self.conexion.commit()

    def closeEvent(self, event):
        self.conexion.close()


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
