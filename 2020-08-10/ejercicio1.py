numero_entero = 10
numero_decimal = 10.4
cadena = "Un texto"
boolean = False

# Un comentario
print(cadena, numero_entero, numero_decimal, boolean)

print("El numero vale {0} {1} {0}".format(numero_entero, numero_decimal))

print(f'El numero vale {numero_entero} {numero_decimal} {boolean} {numero_entero}')