import sqlite3

# Conectar a base de datos (se crea el archivo si no existe)
conexion = sqlite3.connect('2020-11-09/01-base.db')

# Operaciones con la base de datos

cursor = conexion.cursor()
cursor.execute('''CREATE TABLE Usuarios(
                    id INTEGER PRIMARY KEY,
                    nombre_usuario TEXT,
                    nombre_completo TEXT,
                    contraseña TEXT,
                    email TEXT)'''
               )
conexion.commit()

conexion.close()
